/*
 Navicat Premium Data Transfer

 Source Server         : demo
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : excel

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 16/12/2021 14:36:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(50) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '张三', '男', 21, '2000-10-24');
INSERT INTO `user` VALUES (2, '李四', '女', 22, '1999-12-31');
INSERT INTO `user` VALUES (3, '王五', '男', 20, '2001-01-03');
INSERT INTO `user` VALUES (4, '赵六', '女', 23, '1998-10-20');
INSERT INTO `user` VALUES (5, '王五', '男', 19, '2002-01-11');
INSERT INTO `user` VALUES (6, '赵六', '男', 21, '2000-10-31');
INSERT INTO `user` VALUES (7, 'Alice', '女', 27, '1994-10-23');
INSERT INTO `user` VALUES (8, 'Bob', '男', 28, '1993-10-30');

SET FOREIGN_KEY_CHECKS = 1;
